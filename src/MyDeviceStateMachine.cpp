/*----- PROTECTED REGION ID(MyDeviceStateMachine.cpp) ENABLED START -----*/
static const char *RcsId = "$Id:  $";
//=============================================================================
//
// file :        MyDeviceStateMachine.cpp
//
// description : State machine file for the MyDevice class
//
// project :     MyDevice
//
// This file is part of Tango device class.
// 
// Tango is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Tango is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Tango.  If not, see <http://www.gnu.org/licenses/>.
// 
// $Author:  $
//
// $Revision:  $
// $Date:  $
//
// $HeadURL:  $
//
//=============================================================================
//                This file is generated by POGO
//        (Program Obviously used to Generate tango Object)
//=============================================================================

#include <MyDevice.h>

/*----- PROTECTED REGION END -----*/	//	MyDevice::MyDeviceStateMachine.cpp

//================================================================
//  States  |  Description
//================================================================


namespace MyDevice_ns
{
//=================================================
//		Attributes Allowed Methods
//=================================================

//--------------------------------------------------------
/**
 *	Method      : MyDevice::is_Text_allowed()
 *	Description : Execution allowed for Text attribute
 */
//--------------------------------------------------------
bool MyDevice::is_Text_allowed(TANGO_UNUSED(Tango::AttReqType type))
{
	//	Not any excluded states for Text attribute in Write access.
	/*----- PROTECTED REGION ID(MyDevice::TextStateAllowed_WRITE) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	MyDevice::TextStateAllowed_WRITE

	//	Not any excluded states for Text attribute in read access.
	/*----- PROTECTED REGION ID(MyDevice::TextStateAllowed_READ) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	MyDevice::TextStateAllowed_READ
	return true;
}

//=================================================
//		Commands Allowed Methods
//=================================================

}	//	End of namespace
